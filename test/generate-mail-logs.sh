#!/bin/bash

# generates email logs with all but one zipped

# generate a single file
generateFile () {
  seconds=$2
  for i in {0..1000};do
	# for each line randomly do either:
    bool=$(( RANDOM % 3 ))
    if [[ "${bool}" == 1 ]];then
      day=$(date -d "${seconds} seconds ago" +'%b %d %T')
      # add login for user mail0, mail1, mail2, ... mail997, mail998
      user=$((RANDOM % 1000 ))
      echo "${day} vcn ipop3d[25180]: Login user=mail${user} host=24-207-18-129.eastlink.ca [24.207.18.129] nmsgs=0/0" >> "$1"
      seconds=$(( ( RANDOM % 1000 ) + ${seconds} ))
    else
      # add a large random line of text
      head -c 2300 /dev/urandom | base64 >> $1
    fi
  done
  echo ${seconds}
}

file="/var/log/mail.log"

tmp="/tmp/order.log"

seconds=0
# create non zip mail log
seconds=$(generateFile "${tmp}" ${seconds})
generateFile "${file}" ${seconds}
date -d "${seconds} seconds ago"
tac ${tmp} > ${file}
echo "generated ${file} (file 1 of 32)"

rm ${tmp}


# create zip logs
for i in {2..32};do
  base="${file}.${i}"
  seconds=$(generateFile "${tmp}" ${seconds})
  tac ${tmp} > ${base}
  echo $(zip "${base}".gz "${base}") "(file $i of 32)"
  rm "${base}"
  rm "${tmp}"
done
