#!/bin/bash

# This remove the test data created from setup-tests.sh
# This script assumes setup-tests.sh had been called successfully.


# Check if the user is root
if [[ $(whoami) != "root" ]];then
  echo "$0: script is required to run as root"
  exit -1;
fi



# Remove generated data
rm -r /home/test

rm -r "/var/log/freeradius"

rm /var/log/mail.log*

# ends script

echo "Script Completed."
