#!/bin/bash

generateEmail(){
  seconds=0
  size=0;
  for i in {0..10000};do
	# for each line randomly do either:
    bool=$(( RANDOM % 3 ))
    if [[ "${bool}" == 1 ]];then
      day=$(date -d "${seconds} seconds ago" +'%b %d %T')
      
      if [[ ${size} == 4 ]]; then
        # user name to add
		user="mail$((RANDOM % 1000 ))"
	  else
	    # user names to skip:
	    case ${size} in
	    0)
	      user="123dv"
	      ;;
	    1)
	      user="ABC123"
	      ;;
	    2)
	      user="abcdefghijkilmnopqrstuvwxyz"
	    esac
	    size=$(( size + 1 ))
	  fi
	  echo "${day} vcn ipop3d[25180]: Login user=${user} host=24-207-18-129.eastlink.ca [24.207.18.129] nmsgs=0/0"
	  seconds=$(( ( RANDOM % 1000 ) + ${seconds} ))
    else
      # add a large random line of text
      head -c 2300 /dev/urandom | base64
    fi
  done
}

test_log="test-log.txt"

generateEmail > $test_log
time bash ../find-email-login-simple.sh < $test_log

zipped_log="test-log.zip"
zip $zipped_log $test_log 

time bash ../find-email-login-simple.sh < $zipped_log > /dev/null

ls -lh $test_log
rm $test_log
rm $zipped_log
