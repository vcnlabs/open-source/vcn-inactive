#!/bin/bash
# Creates 1000 locked and very expired users (for test)

if [[ $(whoami) != "root" ]];then
  echo "$0: script is required to run as root"
  exit -1;
fi

for i in {0..1000}
do
    useradd "delete$i"
    echo "abc 
abc " | passwd "delete$i"
    chage -E "2011-12-12" "delete$i"
done
