#!/bin/bash

# generates test files for find-active-dailup.sh

folder="/var/log/freeradius/radacct/207.102.64.6"
prefix="auth-detail-"

touch ${folder}/random.txt
echo "Created random name file."

# create the folder to store the logs
if [ -e $folder ];then
   if [ ! -d $folder ];then
      echo "${folder} is not a directory"
      exit -1
   fi
else
   mkdir -p "${folder}"
fi

for idx_file in {0..33};do
  # for each file do:
	file="${folder}/${prefix}$(date -d "${idx_file} days ago" +%Y%m%d)"
	for idx_line in {1..1000}; do
	  # for each line do either:
		data=$(( RANDOM % 3 ))
		if [[ $data == 1 ]]; then
			# add dailup login for test0, test1, ... test998, test999
			user=$(( RANDOM % 1000 ))
			echo "    User-Name = \"test$user\"" >> ${file}
		else
		  # add a long random line of text
			cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 3200 | head -n 1 >> ${file}
		fi
	done
	echo "generated ${file}. (file $((${idx_file} + 1)) of 34)"
done

# add users from the file auth-detail-20171023
cat "${prefix}20171023" >> ${folder}/${prefix}$(date -d "10 day ago" +%Y%m%d)

# add more active users
echo "	User-Name = \"ccc12\"" >> ${folder}/${prefix}$(date +%Y%m%d)
echo "  User-Name = \"last\"" >> ${folder}/${prefix}$(date -d "1 month ago" +%Y%m%d)

# add users that are active 1 month before; these should be shown as active
echo "  User-Name = \"missed\"" >> ${folder}/${prefix}$(date -d "1 month ago 1 day ago" +%Y%m%d)
echo "  User-Name = \"error\"" >> ${folder}/${prefix}$(date -d "1 month ago 2 day ago" +%Y%m%d)
