#!/bin/bash

# Setup tests in local user
# The script setup will create user and create large data files for
# testing.

user="test"

# Part 1: Setup Tests
echo 'Preforming setup tests.'

# test user root
if [[ $(whoami) != "root" ]];then
  echo "$0: script is required to run as root"
  exit -1;
fi

exit_text='exists, exit script to avoid overwrite potentially real data'

# test if freeradius is in use
login="/var/log/freeradius"
if [[ -e $login ]]; then
  echo "$0: $login ${exit_text}"
  exit -1
fi

# test if mail is in use
if [[ -n $(sudo find /var/log/ -name "mail.log.*") ]];then
  echo "$0: /var/log.mail.* ${exit_text}"
  exit -1
fi

# Part 2: Kill All Child Process when Killing Script
childKill(){
	children=$(jobs -p)
	if [[ -n children ]]; then
	  kill $children
	fi
	exit 0
}

trap childKill SIGINT SIGTERM

# Part 3: Create User with Work Folder
user="${user}:${user}"

echo creating user \"$user\"...
mkdir /home/$user

useradd -b /home/ $user

echo 'Creating folder work to store the log folder...'

mkdir /home/$user/work

chown $user ~$user/work

# Part 4: Setup Dialup Log Folder
echo 'Creating folder for generate-auth-files.sh...'

mkdir -p $login"/radacct/207.102.64.6/"

echo "Running the generation scripts in parallel..."

cd $(dirname $0)

# Part 5: Run All Scripts
bash generate-auth-files.sh &
bash generate-mail-logs.sh &
bash generate-test-folders.sh &

# Part 6: End Script
wait

echo "Test data generation completed."
