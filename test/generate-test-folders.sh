#!/bin/bash

# generate some folders and file to test in find-active-websites.sh and find-email-forwards.sh

sub="/home/test"

# website without file ("empty" is not an active user)
mkdir -p $sub/empty/webdata

# user with no email and website ("no" is not an active user)
mkdir $sub/no
touch $sub/no/filler.txt

# user who used to forward his/her emails ("noForward" is an not an active user)
mkdir $sub/noForward
touch $sub/noForward/.forward

# user who use an website ("website" is an active user)
mkdir -p $sub/website/webdata
touch    $sub/website/webdata/index.html

# user who forwards emails ("yes" is an active user)
mkdir          $sub/yes
echo "Hello" > $sub/yes/.forward

# Create 1000 users with website, email forwards, both or none
for i in {1..1000};do
  folder="$sub/user$i"
  mkdir $folder

  # create forward file?
  bool=$(( RANDOM % 2 ))
  if [[ $bool == 1 ]];then
    echo "filler" > $folder/.forward
  fi

  # create website?
  bool=$(( RANDOM % 2 ))
  if [[ $bool == 1 ]]; then
    mkdir $folder/webdata
    touch $folder/webdata/index.html
  fi
done

echo "$0: Test user folders completed."
