#!/bin/bash

# List of variables needed to change before deploy on server

# Used in everywhere
out="logs"

# Used in find-active-websites.sh and find-email-forwards.sh
in_home="/home/"


# Used in fine-email-login.sh and find-email-login-zipped.sh
in_email_folder="/var/log/"

# Part 4: Setup the folders
# If the $out folder doesn't exist,   then      create it
#                    is not a folder, then      return an error
#                                     otherwise do nothing
if [ -e ${out} ];then
	if [ ! -d ${out} ];then
		echo "${out} is not a directory"
		exit -1
	fi
else
	mkdir -p ${out}
fi


function search_email_log {
	output="${1}" #output file
	from_date="${2}" # usually "today", test have different answer

	search=" user=[a-z][a-z0-9]{1,7} "
	today=$(date -d"${from_date}" +%s)
	cutoff=$(date -d"${from_date} 1 month ago" +%s)
	tac | grep -e "${search}" | while IFS='' read -r line || [[ -n "$line" ]]; do
    	at=$(echo "$line" | cut -c 1-7)
		on=$(date -d "${at}" +%s)
		if [[ $today -lt $on ]]; then
		 	on=$(date -d"${at} 1 year ago" +%s)
		fi
		if [[ $cutoff -lt ${on} ]]; then
			echo ${line} | cut -d"=" -f2 | cut -d" " -f1 >> ${output}
		else
		 	echo "Rest of the file is over 1 month."
		 	return
		fi
	done
}
