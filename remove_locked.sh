#!/bin/bash
# Deletes users that are expired over 30 days and password locked.

expire_date=$(( $(date -d "30 day ago" +%s) / 86400))


# for each user where the expire date <= 30 - today && accout expire is not empty && password field starts with "!"
for user in $(awk -F":" '$8 <= '${expire_date}' && $8 != "" && $2~/^\!/{print $1}' /etc/shadow)
do
   userdel -r ${user} &> /dev/null
   if [ $? = 0 ]; then
       echo "$0: deleted ${user} with their mail spool and home directory."
   fi
done
