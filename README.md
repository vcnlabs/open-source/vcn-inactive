# About

## Useful commands

Set expired dates, Lock expired accounts, and remove locked accounts. This does
not update anything, because `-l` option is set.

~~~bash
./active-vcn.sh -l | set_expired.sh "1 year"
./lock_expired
./remove_locked
~~~

Updated dial up and email log (run on daily):
~~~bash
./active-vcn.sh
~~~

Update dial up log, email log, forwarding emails logs, websites list (run
weekly):

~~~bash
./active-vcn.sh -m
~~~

Updated all active users log (run initially and when there are problem):

~~~bash
./active-vcn.sh -f
~~~

See more help on `active-vcn.sh`:

~~~bash
./active-vcn.sh -h
~~~

## List of administration scripts

[active-vcn.sh](active-vcn.sh)
    runs bash prefixed with `find-` in parallel with options. The result can be
    found in `log/all_active_users.log`

[find-active-dailup.sh](find-active-dailup.sh)
    go into dialup logs and search for user login in. The result can be found in
    `log/active_dailup_access.log`

Code can used to set expired account with:

~~~bash
./find-active-dailup.sh | ./set_expired.sh "1 year"
~~~

[find-active-websites.sh](find-active-websites.sh)
    go into the home folder and search for websites. The result can be found in
    `log/active_websites.log`

Code can used to set expired account with:

~~~bash
./find-active-websites.sh | ./set_expired.sh "1 year"
~~~

[find-email-forwards.sh](find-email-forwards.sh)
    go into the home folder and search for forwarding file. Code can used to set
    expired account with:

~~~bash
./find-email-forwards.sh | ./set_expired.sh "1 year"
~~~

[find-email-login-simple](find-email-login-simple.sh)
	read from standard in (can be text or zipped file) to search for email
	login. Code can use to set expired account with:

~~~bash
./find-email-login-simple < ${file}
~~~


[find-email-login.sh](find-email-login.sh)
    go into `/var/log/main.log` and search for email login Code can used to set
    expired account with:

~~~bash
./find-email-login.sh | ./set_expired.sh "1 year"
~~~

[find-email-login-zipped.sh](find-email-login-zipped.sh)
    go into the `/var/log/` and search for email login in zipped log 2 to 59.
    Code can used to set expired account with:

~~~bash
./find-email-login.sh
cat log/active_email_logins.[0-5].log | ./set_expired.sh "1 year"
~~~

[set_expired.sh](set_expired.sh)
    set users' expired date. Run with the command
    `echo $user | sudo bash set_expired.sh $date`

[lock_expired.sh](lock_expired.sh)
    find users with expired date and lock their password

[remove_locked.sh](remove_locked.sh)
    find users locked for 30 days
