#!/bin/bash

# Part 1: Script Preparation

cd "$(dirname $0)"

source shared_variables.sh

# Part 2: Setup More Variables

# out => from "source .shared_variables.sh"
out_log="${out}/active_dailup_access.log"

in_folder="/var/log/freeradius/radacct/207.102.64.6/" #log file location

search='User-Name = \"[a-zA-Z0-9]{2,8}\"$' # what to find in the log file

end_date=$(date -d "1 month ago" +%s) # end date in unix epoch time

## Part 3: gather the logs to search
logs=""
# command copied and modified from the file `activevcn` line 87
for log_file in $(ls ${in_folder});do
	if [[ "$log_file" == "auth-detail"* ]]; then

		# log_date = log_file date in unix epoch time
		log_date=$(date -d ${log_file:12:4}"-"${log_file:16:2}"-"${log_file:18} +%s)

		if [[ end_date -lt log_date ]];then
			# only search log if before the cut off date
			logs="${in_folder}/${log_file} ${logs}"
		fi
	fi
done

## Part 4: Run the command
egrep -h "${search}" ${logs} | cut -d"\"" -f2 | sort -u > ${out_log}
sort -u ${out_log}
