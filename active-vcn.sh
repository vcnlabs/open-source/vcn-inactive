#!/bin/bash

# This script has proper usage message. Run by call ./active-vch.sh -h

# Part 1: Script Preparation
cd "$(dirname $0)"

source shared_variables.sh

# Part 2: define functions
# list users. This is last step of the script, except for option errors
# and option "-h" or "-help"
list(){
  if [ -z "$(ls $out)" ];then
    echo "${0}: No logs file to sort"
    exit -1
  fi
  sort -mu $out/* | tee "${out}/all_active_users.log"
}


# print help text
helper(){
  cat <<EOF
Usage: vcn-active.sh [option]
Search and list active vcn users.

Options (Quick Output):
  -h, --help      print this help and exit
  -l, --list      unique sort users in all the log files and exit

Options (Update Active Users):
  -b, --basic     (DEFAULT) search dialup logins and text email log
  -m, --medium    like -s and also search home folder
  -f, --full      like -m and also search in zipped email logs

There are 5 find scripts that are being using in active-vcn.sh:
  find-active-dailup.sh:      search dialup logins,     runs by default
  find-email-login.sh:        search email login,       runs by default
  find-email-login-zipped.sh: search past email logins, runs with -a
  find-email-forwards.sh:     serach email forwarding,  runs with -a / -f
  find-active-websites.sh:    search active websites,   runs with -a / -f

The active users lists are stored in ${out}. The scripts themselves runs in
parallel.

EOF
}

# Part 3: read options
# option list:
OPTIONS=hlbmf
LONGOPTIONS=help,list,basic,medium,full

# parse options
PARSED=$(getopt -o "${OPTIONS}" -l "${LONGOPTIONS}" --name "${0}" -- "${@}")

# error option detected
if [[ ${?} -ne 0 ]]; then
  echo
  helper
  exit 2
fi

# parse options (cont.)
eval set -- "${PARSED}"

# read options
logzips=false
folder=false

while true; do
  case "${1}" in
    -h|--help)
      helper
      exit 0
      ;;
    -l|--list)
      list
      exit 0
      ;;
    -b|--basic)
	  logzips=0
      folder=0
      shift
      ;;
    -m|--medium)
	  logzips=0
      folder=1
      shift
      ;;
    -f|--full)
      logzips=1
      folder=1
      shift
      ;;
    --)
      shift
      break
      ;;
  esac
done

# Part 4: Run serach

# runs only if -a is set
if [ ${logzips} = 1 ]; then
  bash find-email-login-zipped.sh &> /dev/null &
  pid_list+=" $!"
fi

# runs only if -f, -a or both is set
if [ ${folder} = 1 ]; then
  bash find-active-websites.sh &> /dev/null &
  pid_list+=" $!"
  bash find-email-forwards.sh &> /dev/null &
  pid_list+=" $!"
fi

# runs default scripts
bash find-email-login.sh &> /dev/null &
pid_list+=" $!"
bash find-active-dailup.sh &> /dev/null &
pid_list+=" $!"

trap "kill ${pid_list}" SIGINT

wait ${pid_list}

# final output
list
