#!/bin/bash
# find users that expired

expire_date=$(( $(date +%s) / 86400))

#  field     if expire <= today && expire is not empty
for user in $(awk -F":" '$8 <= '${expire_date}' && $8 != "" {print $1}' /etc/shadow)
do
	passwd -l ${user} &> /dev/null
	if [ $? = 0 ]; then
		echo "${0}: locked password of ${user} account."
	fi
done
