#!/bin/bash

# Part 1: Script Preparation

cd "$(dirname $0)"

source shared_variables.sh

# Part 2: Setup More Variables

# out => from "source .shared_variables.sh"
out_log="${out}/active_email_forwards.log"

# in_home => from "source .shared_variables.sh"

search='.forward'

#Part 3: Run Command
# command copied and modified from the file `activevcn` line 69
find "${in_home}" -name "${search}" -type f -not -empty | cut -d "/" -f4 | \
	sort -u > "${out_log}"

sort -u "${out_log}"
