#!/bin/bash

# Part 1: Script Preparation

cd "$(dirname $0)"

source shared_variables.sh

# Part 2: Setup More Variables

# out => from "source .shared_variables.sh"
out_log="${out}/active_websites.log"

# in_home => from "source .shared_variables.sh"
search='webdata'

#Part 3: Run Command
# command copied and modified from the file `activevcn` line 72
# could not get command in the file `activevcn` line 92 to work
find ${in_home} -name ${search} -type d -not -empty| cut -d "/" -f4 | \
	sort -u > ${out_log}

sort -u ${out_log}
