#!/bin/bash

# Set the expire date ${1} in date function

if [ $# -ne 1 ];then
	echo "${0} Usage: set_expired.sh [expire date]"
fi

while read user;do
	chage -E $(date -d "${1}" +%Y-%m-%d) ${user}
	if [ $? = 0 ];then
		echo "$0: set ${user} to expire in ${1}"
	fi
done
