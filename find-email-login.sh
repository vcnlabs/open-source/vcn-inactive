#!/bin/bash

# Part 1: Script Preparation

cd "$(dirname $0)"

source shared_variables.sh

# Part 2: Setup More Variables

# out => from "source .shared_variables.sh"
out_tmp="/tmp/mail.log"
out_log="${out}/active_email_logins.log"

in_log="${in_email_folder}mail.log"

#Part 3: Run Command
# function is in shared_variables
# basic test script is test/mail-test.sh
cat  "${in_log}" | search_email_log "${out_tmp}" 'today' > /dev/null

sort -u "${out_tmp}" > "${out_log}"
rm "${out_tmp}"

cat ${out_log}
