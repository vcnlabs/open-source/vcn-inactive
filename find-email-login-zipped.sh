#!/bin/bash

# function is in shared_variables
# basic test script is test/mail-test-zipped.sh

# Part 1: Script Preparation

cd "$(dirname $0)"

source shared_variables.sh

# Part 2: Setup More Variables

# out => from "source .shared_variables.sh"
out_log_prefix="$out/active_email_logins."
out_log_postfix=".log"
#out_log => $out_log_prefix$i$out_log_postfix

in_log_prefix="${in_email_folder}mail.log."
in_log_postfix=".gz"
#in_logzip => $in_log_prefix$i$out_log_postfix    or
#          => $in_log_prefix$out_log_postfix

find_email_logins='Login user='

#Part 3: Run Command for Logs File with < 10
# Check email zipped logs (numbered 0-9) for user names
out_tmp="/tmp/mail-zipped.log"
out_log="${out_log_prefix}0$out_log_postfix"
if [ -e ${out_log} ];then
    rm "${out_log}"
fi

for i in {2..9}; do
    in_logzip=$(ls ${in_log_prefix}${i}${in_log_postfix} 2> /dev/null) # check if file exists
    if [ $? == 0 ]; then
        cur=$(zcat ${in_logzip} | search_email_log ${out_tmp} 'today')
        echo "${0}: Serached " ${in_logzip}
        if [ -n cur ]; then
            sort -u ${out_tmp} > ${out_log}
            rm ${out_tmp}
            exit 0
        fi
    fi
done
sort -u ${out_tmp} > ${out_log}
rm ${out_tmp}

#Part 3: Run Command for Logs File with 2 Digits
# Check email zipped logs (numbered 11-59) for user names
count=0 #file count
for i in {1..5}; do
  out_log="$out_log_prefix$i$out_log_postfix"
  if [ -e ${out_log} ];then
      rm "${out_log}"
  fi
  for j in {0..9}; do
      in_logzip=$(ls $in_log_prefix$i$j$in_log_postfix 2> /dev/null) # check if file exists
      if [ $? == 0 ]; then # if `ls` command is successful and no error thrown last line
          cur=$(zcat ${in_logzip} | search_email_log ${out_tmp} 'today')
          echo "${0}: Serached " ${in_logzip}
          if [ -n cur ]; then
              sort -u ${out_tmp} > ${out_log}
              rm ${out_tmp}
              exit 0
          fi
      fi
  done
  sort -u ${out_tmp} > ${out_log}
  rm ${out_tmp}
done
