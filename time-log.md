

## Time recorded the [script to find active dialup](find-achive-dialup)
~~~
real	0m0.187s
user	0m0.080s
sys		0m0.032s
~~~

## Time recorded the [script to find active websites](find-active-websites.sh)
~~~
real	0m0.498s
user	0m0.020s
sys		0m0.040s
~~~

## Time recorded the [script to find forwarded email accounts](find-email-forwards.sh)
~~~
real	0m0.064s
user	0m0.012s
sys		0m0.040s
~~~

## Time recorded the [script to find account in email log](find-email-login.sh)
~~~
real	0m0.009s
user	0m0.000s
sys		0m0.000s
~~~

## Time recorded the [script to find account in email zipped log](find-email-login-zipped.sh)
~~~
real	0m1.021s
user	0m0.772s
sys		0m0.044s
~~~

## Time recorded the [script to lock expired accounts](lock-expired.sh)


Tested on Oct 31, 2017
~~~
real	1m21.400s
user	0m5.192s
sys 	0m3.120s
~~~


## Time recorded the [script to remove locked accounts](remove-locked.sh)

This is done after calling `sudo locked-expired.sh`

Tested on Oct 19, 2017
~~~
real	8m44.515s
user	0m17.836s
sys 	0m19.216s
~~~

Tested on Oct 31, 2017
~~~
real	6m44.238s
user	0m14.776s
sys 	0m14.296s
~~~
